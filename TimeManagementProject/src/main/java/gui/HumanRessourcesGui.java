package gui;

import java.io.IOException;
import java.net.URL;

import javax.swing.JOptionPane;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class HumanRessourcesGui {
	
	private static Stage stage = new Stage();
	private static Scene scene;
	
	public static void changeSceneHR() throws IOException {
		URL url = MainGUI.class.getResource("/HrLogin.fxml");
		FXMLLoader fxmlLoader = new FXMLLoader(url);
		Parent p = fxmlLoader.load();		
		scene = new Scene(p, 800, 600);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.setTitle("Human Ressources Login");
		stage.show();	
	}
}

package gui;
import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainGUI extends Application {
	
	public MainGUI() {
		
	}

	private static Scene scene;
	public Stage stage = new Stage();
	
	public void start(Stage stage) throws IOException {
		this.stage = stage;
		URL url = MainGUI.class.getResource("/TimeManagement.fxml");
		FXMLLoader fxmlLoader = new FXMLLoader(url);
		Parent p = fxmlLoader.load();
		scene = new Scene(p, 800, 600);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.setTitle("Time Managment Software STC");
		stage.show();
	}

	static void setRoot(String fxml) throws Exception {
		scene.setRoot(loadFXML(fxml));
	}

	private static Parent loadFXML(String fxml) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(MainGUI.class.getResource(fxml + ".fxml"));
		return fxmlLoader.load();
	}

	public static void main(String[] args) {
		launch();
	}

	public static void closeProgram() {
		System.exit(0);
	}
}
package programSpace;

import java.sql.SQLException;
import java.time.LocalDate;
import javax.swing.JOptionPane;
import gui.EmployeeGui;
import gui.HumanRessourcesGui;
import gui.MainGUI;
import gui.SupervisorGui;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class JFXcontroller {

	@FXML
	public TextField usernameField = new TextField();
	@FXML
	public PasswordField passwordField = new PasswordField();
	@FXML
	public Button employeeLogout = new Button();
	@FXML
	public Button supervisorLogout = new Button();
	@FXML
	public Label createEmployeeLabel = new Label();
	@FXML
	public TextField newNameHR = new TextField();
	@FXML
	public TextField newPasswordHR = new TextField();
	@FXML
	public Button hrLogout = new Button();
	@FXML
	public TextArea hrComment = new TextArea();
	@FXML
	public TableColumn<LocalDate, String> dateColumn = new TableColumn<LocalDate, String>();
	@FXML
	public DatePicker employeeDateFrom = new DatePicker();
	@FXML
	public DatePicker employeeDateTo = new DatePicker();
	@FXML
	public TextField employeeName = new TextField();
	@FXML
	public DatePicker hrDateFrom = new DatePicker();
	@FXML
	public DatePicker hrDateTo = new DatePicker();
	@FXML
	public TextField superEmployeeDisapprove = new TextField();
	@FXML
	public TextArea superListEmployee = new TextArea();
	@FXML
	DatePicker supervisorDateFrom = new DatePicker();

	private static String name, password;
	private JDBCcontroller jdbc = new JDBCcontroller();

	@FXML
	public boolean employeeLogin() {
		try {
			if (jdbc.checkUserForGroupEmployee(usernameField.getText())
					&& jdbc.createConnectionToDatabase(usernameField.getText(), passwordField.getText()) != null) {
				name = usernameField.getText();
				password = passwordField.getText();
				EmployeeGui.changeSceneEmployee();
				jdbc.leaveConnectionToDatabase();
			} else
				JOptionPane.showInternalMessageDialog(null, "Wrong Login");
		} catch (Exception e) {
			JOptionPane.showInternalMessageDialog(null, "Wrong Login");
		}
		return true;
	}

	@FXML
	public void humanRessourcesLogin() {
		try {
			if (jdbc.checkUserForGroupHR(usernameField.getText())
					&& jdbc.createConnectionToDatabase(usernameField.getText(), passwordField.getText()) != null) {
				jdbc.createConnectionToDatabase(usernameField.getText(), passwordField.getText());
				name = usernameField.getText();
				password = passwordField.getText();
				HumanRessourcesGui.changeSceneHR();
				jdbc.leaveConnectionToDatabase();
			} else
				JOptionPane.showInternalMessageDialog(null, "Wrong Login");
		} catch (Exception e) {
			JOptionPane.showInternalMessageDialog(null, "Wrong Login");
		}
	}

	@FXML
	public void supervisorLogin() {
		try {
			if (jdbc.checkUserForGroupSupervisor(usernameField.getText())
					&& jdbc.createConnectionToDatabase(usernameField.getText(), passwordField.getText()) != null) {
				jdbc.createConnectionToDatabase(usernameField.getText(), passwordField.getText());
				name = usernameField.getText();
				password = passwordField.getText();
				SupervisorGui.changeSceneSupervisor();
				jdbc.leaveConnectionToDatabase();
				superListEmployee.setEditable(false);
			} else
				JOptionPane.showInternalMessageDialog(null, "Wrong Login");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void employeeLogout() {
		Stage stage = (Stage) employeeLogout.getScene().getWindow();
		stage.close();
	}

	@FXML
	public void employeeSubmit() throws SQLException {
		jdbc.createDatabaseForNewUser(name);
		if (employeeDateTo.getValue() == null) {
			jdbc.pushDateToDatabase(employeeDateFrom.getValue(), name, password);
		} else {
			LocalDate date = employeeDateFrom.getValue();
			while (date.isBefore(employeeDateTo.getValue())) {
				jdbc.pushDateToDatabase(date, name, password);
				date = date.plusDays(1);
			}
			jdbc.pushDateToDatabase(employeeDateTo.getValue(), name, password);
		}
	}

	@FXML
	public void insertEmployee() throws SQLException {
		jdbc.createNewUser(newNameHR.getText(), newPasswordHR.getText());
		jdbc.insertUserToEmployedTable(newNameHR.getText());
		JOptionPane.showInternalMessageDialog(null, "User: " + newNameHR.getText() + " created.");
	}

	@FXML
	public void insertHR() throws SQLException {
		jdbc.createNewUser(newNameHR.getText(), newPasswordHR.getText());
		jdbc.insertUserToHrTable(newNameHR.getText());
		jdbc.insertUserToEmployedTable(newNameHR.getText());
		JOptionPane.showInternalMessageDialog(null, "HR: " + newNameHR.getText() + " created.");

	}

	@FXML
	public void insertSupervisor() throws SQLException {
		jdbc.createNewUser(newNameHR.getText(), newPasswordHR.getText());
		jdbc.insertUserToSupervisorTable(newNameHR.getText());
		jdbc.insertUserToEmployedTable(newNameHR.getText());
		JOptionPane.showInternalMessageDialog(null, "Supervisor: " + newNameHR.getText() + " created.");

	}

	@FXML
	public void hrDateSubmit() throws SQLException {
		if (name.equals(employeeName.getText())) {
			JOptionPane.showInternalMessageDialog(null, "You cannot give yourself sick-days/vacation.");
		} else {
			LocalDate date = hrDateFrom.getValue();
			if (hrDateTo.getValue() == null) {
				jdbc.pushDateToDatabaseHR(date, employeeName.getText(), hrComment.getText());
			} else {
				while (date.isBefore(hrDateTo.getValue())) {
					jdbc.pushDateToDatabaseHR(date, employeeName.getText(), hrComment.getText());
					date = date.plusDays(1);
				}
			}
		}
		hrComment.setText("");
	}

	@FXML
	public void supervisorDateEntry() throws SQLException {
		if (name.equals(employeeName.getText())) 
			JOptionPane.showInternalMessageDialog(null, "You cannot dis-/approve your own workingtimes.");
		else 
			jdbc.supervisorDateRetrieve(employeeName.getText(), Integer.parseInt(superEmployeeDisapprove.getText()), supervisorDateFrom.getValue(), hrComment.getText());
	}

	@FXML
	public void supervisorListEmployee() throws SQLException {
		superListEmployee.setText(jdbc.listAllEntriesOfTable(employeeName.getText()));
	}
	
	@FXML
	public void listEmployeeTable() throws SQLException {
		superListEmployee.setText(jdbc.listAllEntriesOfTable(name));
	}

	@FXML
	public void hrLogoutButton() {
		Stage stage = (Stage) newNameHR.getScene().getWindow();
		stage.close();
	}

	@FXML
	public void hrClose() {
		MainGUI.closeProgram();
	}

	@FXML
	public void employeeClose() {
		MainGUI.closeProgram();
	}

	@FXML
	public void supervisorLogout() {
		Stage stage = (Stage) supervisorLogout.getScene().getWindow();
		stage.close();
	}

	@FXML
	public void supervisorClose() {
		MainGUI.closeProgram();
	}
}
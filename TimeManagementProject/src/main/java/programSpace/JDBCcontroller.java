package programSpace;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

public class JDBCcontroller {
		
	private static Connection connection;
	private Statement statement;
	private String root = "root", rootPassword = "";
	private static ResultSet resSet;
	
	private String urlDB = "jdbc:mysql://localhost:3306/softwareengineering";	
	
	public Connection createConnectionToDatabase(String name, String password) {
		try {
			Connection con = DriverManager.getConnection(urlDB, name, password);
			System.out.println("Login successful");
			return con;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Login failed");
		return null;
	}
	
	public void createNewUser(String name, String password) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		String query = "CREATE USER " + name + "@'%' IDENTIFIED BY '" + password + "';";
		statement = connection.prepareStatement(query);
		statement.executeUpdate(query);
		query = " GRANT ALL PRIVILEGES ON *.* TO '"+name+"'@'%' IDENTIFIED BY '"+password+"' WITH GRANT OPTION;";
		statement.executeUpdate(query);
		leaveConnectionToDatabase();
	}
	
	public void pushDateToDatabase(LocalDate date, String name, String password) throws SQLException {
		Connection connection = createConnectionToDatabase(name, password);
		String query = "INSERT IGNORE INTO softwareengineering." + name + "(Approved, WorkTable, Comment) VALUES (0,'" + date.toString() + "','')";
		statement = connection.prepareStatement(query);
		statement.executeUpdate(query);
		leaveConnectionToDatabase();
	}
	
	public void pushDateToDatabaseHR(LocalDate date, String name, String comment) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		String query = "INSERT IGNORE INTO softwareengineering." + name + "(Approved, WorkTable, Comment) "
					+ "VALUES (0,'" + date.toString() + "','" + comment + "')";
		statement = connection.prepareStatement(query);
		statement.executeUpdate(query);
		leaveConnectionToDatabase();
	}
	
	public void createDatabaseForNewUser(String tablename) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		String query = "CREATE TABLE IF NOT EXISTS " + tablename
				+ " (Approved INTEGER, "
				+ " WorkTable DATE, "
				+ " Comment VARCHAR(255))";
		statement = connection.prepareStatement(query);
		statement.execute(query);
		leaveConnectionToDatabase();
	}
	
	public void insertUserToEmployedTable(String name) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		String query = "INSERT IGNORE INTO employed (name) SELECT * FROM (SELECT '"+name+"') "
				+ "AS tmp WHERE NOT EXISTS ( SELECT name FROM employed WHERE name = '"+name+"' ) LIMIT 1;";
		statement = connection.createStatement();
		statement.executeUpdate(query);
		leaveConnectionToDatabase();
	}
	
	public void insertUserToHrTable(String name) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		String query = "INSERT IGNORE INTO hr (name) SELECT * FROM (SELECT '"+name+"') "
				+ "AS tmp WHERE NOT EXISTS ( SELECT name FROM hr WHERE name = '"+name+"' ) LIMIT 1;";
		statement = connection.createStatement();
		statement.executeUpdate(query);
		leaveConnectionToDatabase();
	}
	
	public void insertUserToSupervisorTable(String name) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		String query = "INSERT IGNORE INTO supervisor (name) SELECT * FROM (SELECT '"+name+"') "
				+ "AS tmp WHERE NOT EXISTS ( SELECT name FROM supervisor WHERE name = '"+name+"' ) LIMIT 1;";
		statement = connection.createStatement();
		statement.executeUpdate(query);
		leaveConnectionToDatabase();
	}
	
	public boolean checkUserForGroupEmployee(String name) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		ResultSet res = resSet;
		statement = connection.createStatement();
		res = statement.executeQuery("SELECT * FROM `employed` WHERE name='" + name + "'");
		if(res.next()) {
			return true;			
		}
		return false;
	}
	
	public boolean checkUserForGroupHR(String name) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		ResultSet res = resSet;
		statement = connection.createStatement();
		res = statement.executeQuery("SELECT * FROM `hr` WHERE name='" + name + "'");
		if(res.next()) {
			return true;			
		}
		return false;
	}
	
	public boolean checkUserForGroupSupervisor(String name) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		ResultSet res = resSet;
		statement = connection.createStatement();
		res = statement.executeQuery("SELECT * FROM `supervisor` WHERE name='" + name + "'");
		if(res.next()) {
			return true;			
		}
		return false;
	}
	
	public String listAllEntriesOfTable(String table) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		ResultSet res = resSet;
		statement = connection.createStatement();
		res = statement.executeQuery("SELECT * FROM " + table + ";");
		String output = "\tApproved \t Worktable \t\t Comment \n\n";
		while(res.next()) {
			output +="\t" + res.getInt("Approved") + " \t\t\t " + res.getDate("WorkTable") + " \t\t " + res.getNString("Comment") + 
					"\n ____________________________________________________________________\n";
			
		}
		return output;
	}
	
	public void supervisorDateRetrieve(String name, int approval, LocalDate date, String comment) throws SQLException {
		Connection connection = createConnectionToDatabase(root, rootPassword);
		String query = "UPDATE softwareengineering."+name+" SET Approved='"+approval+"' , Comment='" + comment + "' WHERE WorkTable='"+date+"'";
		statement = connection.createStatement();
		statement.executeUpdate(query);
	}
	
	public void leaveConnectionToDatabase() throws SQLException {
		if(connection != null) {
			try {
				connection.close();				
			}catch(Exception e) {
				System.out.println(connection + " left open (connection)" );
				e.printStackTrace();
			}
		}
		if(statement != null) {
			try {
				statement.close();
				
			}catch(Exception e) {
				System.out.println(statement + " left open (statement)" );
				e.printStackTrace();				
			}
		}
		if(resSet != null) {
			try {
				resSet.close();
			} catch(Exception e) {
				System.out.println(resSet + " left open (result set)");
				e.printStackTrace();
			}
		}
	}
}
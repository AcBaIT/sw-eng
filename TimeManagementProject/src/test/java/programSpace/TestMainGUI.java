package programSpace;

import java.util.concurrent.TimeoutException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import gui.MainGUI;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

public class TestMainGUI  extends ApplicationTest {
	Stage stage;

	@Before
	public void setUpClass() throws Exception {
		ApplicationTest.launch(MainGUI.class);
	}
	
	@After
	public void afterEachTest() throws TimeoutException {
		FxToolkit.hideStage();
		release(new KeyCode[] {});
	}
	
	@Test
	public void testEmployeeScenario() throws InterruptedException, TimeoutException {
		clickOn("#usernameField").eraseText(20).write("bob");
		clickOn("#passwordField").eraseText(1).write("1");
		clickOn("#employeeButton").targetWindow("Employee Login");
		clickOn("#employeeLogout");
		clickOn("#passwordField").eraseText(1);
		clickOn("#employeeButton");
	}
	
	@Test
	public void testSupervisorScenario() throws InterruptedException, TimeoutException {
		clickOn("#usernameField").eraseText(20).write("employee");
		clickOn("#passwordField").eraseText(1).write("1");
		clickOn("#supervisorButton").targetWindow("Supervisor Login");
		clickOn("#employeeName").write("employee");
		clickOn(750.0,440.0);
		clickOn("#supervisorLogout");
		clickOn("#passwordField").eraseText(1);
		clickOn("#supervisorButton");	
	}
	
	@Test
	public void testHrScenario() throws InterruptedException, TimeoutException {
		clickOn("#usernameField").write("hrAcc");
		clickOn("#passwordField").write("1");
		clickOn("#humanRessourcesButton").targetWindow("Human Ressources Login");
		clickOn("#employeeName").write("employee");
		clickOn("#supervisorListEmployee");
		clickOn(540,265).clickOn(470,300).clickOn(470,400);
		clickOn("#hrComment").write("sick this day");
		//clickOn("#hrSickDaySubmit"); //uncomment for official test
		clickOn("#hrLogout");
		clickOn("#passwordField").eraseText(1);
		clickOn("#humanRessourcesButton");
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		// TODO Auto-generated method stub
	}

}